# blog_PythonDjango

My blog w/ python and django -> deployed on pythonanywhere.com

*http://roxannepons.pythonanywhere.com/*

## To acces the post edit function:

1. Go to *http://roxannepons.pythonanywhere.com/admin*
2. Log in
3. Go back to the app and an pencil icon should be present to edit the text.